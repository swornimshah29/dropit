package com.kawadi.swornim.dropit.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.kawadi.swornim.dropit.R;

import java.util.HashMap;
import java.util.Map;

public class MainActivity2 extends AppCompatActivity implements LocationListener {
    private LocationManager locationManager;
    private TextView details;
    private FusedLocationProviderClient mFusedLocationClient;
    private boolean gpsSettingsOn = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        details=findViewById(R.id.logoName);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);


        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i("mytag", "All location settings are satisfied.");
                        //location is already on find the current location
                        getCurrentLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i("mytag", "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(MainActivity2.this, 0x1);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i("mytag", "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        showGpsSettings();
                        Log.i("mytag", "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });




        /*
         BY default the ordering is ascending so setting from latest timestamp and limit 1 means latest single data

        */
        FirebaseFirestore.getInstance().collection("pickers").orderBy("timestamp", Query.Direction.DESCENDING).limit(1).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot documentSnapshots) {
                if (documentSnapshots != null) {
                    for (DocumentSnapshot each : documentSnapshots.getDocuments()) {
//                        Log.i("mytag", each.getData() + "");

                    }
                }

            }
        });



    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case 0x1:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i("mytag", "User agreed to on the location");
                        //find the current location
                        getCurrentLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i("mytag", "User didnt not aggreed to on the location");
                        getCurrentLocation();
                        break;
                }
                break;


             case 100:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i("mytag", "User agreed to on the location");
                        getCurrentLocation();

                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i("mytag", "User didnt not aggreed to provide location permission");
                        break;
                }
                break;

        }
    }


    @Override
    public void onLocationChanged(Location location) {
           Log.i("mytag", location + "");
            locationManager.removeUpdates(this);
            Toast.makeText(getApplicationContext(), "Location found successfully", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {
        Log.i("mytag", s + " provider enabled");

    }

    @Override
    public void onProviderDisabled(String s) {
        Log.i("mytag", s + " provider disabled");

    }

    private void showGpsSettings() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Location Settings");
        alertDialog.setMessage("Allow location for KAWADI");
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                gpsSettingsOn = true;
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showGpsSettings();

            }
        });
        alertDialog.setCancelable(true);
        alertDialog.show();


    }


    private void getCurrentLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(MainActivity2.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                        100);//request to determine which code is what type of request

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.

        }else{
            //permission has been granted

            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            Log.i("mytag","mfusedlocationclient  called success");


                            if(location==null){


//                                Toast.makeText(getApplicationContext(), "Location is null", Toast.LENGTH_LONG).show();
//                                Toast.makeText(getApplicationContext(), "Please once connect to the internet for a minute", Toast.LENGTH_LONG).show();


                            }else {
                                // Got last known location. In some rare situations this can be null.

                                Log.i("mytag","USER_CURRENT_LOCATION_LAT "+location.getLatitude());
                                Log.i("mytag","USER_CURRENT_LOCATION_LON "+location.getLongitude());

                                String latitude=String.valueOf(location.getLatitude());
                                String longitude=String.valueOf(location.getLongitude());
                                String url="https://maps.googleapis.com/maps/api/elevation/json?locations="+location.getLatitude()+","+location.getLongitude()+"&key=AIzaSyDFbkl6JHc8L5yhGUYhya602rND1EIIN9g";
                                Log.i("mytag","url "+url);
                                Log.i("mytag","url "+"https://maps.googleapis.com/maps/api/elevation/json?locations=27.6788727,85.364918&key=AIzaSyCk5m--bRDRIO-lxrrIp-Y_3QJuyBgh-2I");

                                RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
                                StringRequest stringRequest=new StringRequest(Request.Method.GET,
//                IMetaData.serverUrl+"/authentication.php",
//                                        "https://maps.googleapis.com/maps/api/elevation/json?locations=27.6788727,85.364918&key=AIzaSyCk5m--bRDRIO-lxrrIp-Y_3QJuyBgh-2I",
                                        url,
                                        new com.android.volley.Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                Log.i("mytag","rating response "+response);
                                                details.setText(response);
                                            }
                                        }, new com.android.volley.Response.ErrorListener() {


                                    @Override
                                    public void onErrorResponse(VolleyError error) {

                                    }
                                }){
                                    @Override
                                    protected java.util.Map<String, String> getParams() throws AuthFailureError {
                                        Map<String,String> map=new HashMap<>();

                                        return map;
                                    }


                                };

                                requestQueue.add(stringRequest);



                            }
                        }
                    });
            Log.i("mytag","Permission has been granted for both fine and coarse");
            /*Last known location might never be the current location  so requesting for a new location explicitly*/
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            try{
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000,
                        5, this);

                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000,
                        5, this);
            }catch (Exception e){

            }

        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        if(gpsSettingsOn){
            getCurrentLocation();
            gpsSettingsOn=false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

    }
}

