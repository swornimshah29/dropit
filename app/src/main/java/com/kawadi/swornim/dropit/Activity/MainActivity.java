package com.kawadi.swornim.dropit.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBufferResponse;
import com.google.android.gms.location.places.PlacePhotoMetadataResponse;
import com.google.android.gms.location.places.PlacePhotoResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.kawadi.swornim.dropit.DataStructure.Users;
import com.kawadi.swornim.dropit.Fragments.FCamera;
import com.kawadi.swornim.dropit.Fragments.FMap;
import com.kawadi.swornim.dropit.Fragments.FPlacesSelection;
import com.kawadi.swornim.dropit.Fragments.FSlider;
import com.kawadi.swornim.dropit.R;
import com.kawadi.swornim.dropit.Services.CustomSharedPref;
import com.nileshp.multiphotopicker.photopicker.activity.PickImageActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static ViewPager viewPager;
    private PagerAdapter mPagerAdapter;
    public static List<Users> users = new ArrayList<>();
    private FloatingActionButton dropFAB;
    private List<String> pathList = new ArrayList<>();
    private ImageView userProfile;
    private List<Fragment> fragments = new ArrayList<>();

    private Handler uploadHandler = new Handler();
    private List<ProgressManager> progressManagers = new ArrayList<>();
    private StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("users_profile_pictures");
    private boolean handlerRan = false;
    public static Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);//R.resouceType.resourceName
        context = getApplicationContext();
        fragments.add(new FSlider());
        fragments.add(new FSlider());
        fragments.add(new FSlider());
        fragments.add(new FSlider());
        fragments.add(new FSlider());
        fragments.add(new FCamera());
        fragments.add(new FMap());
        fragments.add(new FPlacesSelection());
        dropFAB = findViewById(R.id.dropFAB);
        dropFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mIntent = new Intent(getApplicationContext(), PickImageActivity.class);
                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 5);
                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
                startActivityForResult(mIntent, PickImageActivity.PICKER_REQUEST_CODE);

            }
        });

        viewPager = findViewById(R.id.fslider_main_id);
        getMoments();

        GeoDataClient geoDataClient = Places.getGeoDataClient(this, null);
        PlaceDetectionClient placeDetectionClient = Places.getPlaceDetectionClient(this, null);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        placeDetectionClient.getCurrentPlace(null).addOnCompleteListener(new OnCompleteListener<PlaceLikelihoodBufferResponse>() {
            @Override
            public void onComplete(@NonNull Task<PlaceLikelihoodBufferResponse> task) {


                for(PlaceLikelihood placeLikelihood:task.getResult()){
                    Log.i("mytag","places name "+placeLikelihood.getPlace().getName()+"");
                    Log.i("mytag","place likelihood "+placeLikelihood.getLikelihood()+"");
                    Log.i("mytag","place url "+placeLikelihood.getPlace().getWebsiteUri()+"");
                    Log.i("mytag","place attributes "+placeLikelihood.getPlace().getAttributions()+"");
                    Log.i("mytag","place phonenumber "+placeLikelihood.getPlace().getPhoneNumber()+"");
                    Log.i("mytag","place id "+placeLikelihood.getPlace().getId()+"");
                    Log.i("mytag","place address "+placeLikelihood.getPlace().getAddress()+"");
                    Log.i("mytag","place placetypes "+placeLikelihood.getPlace().getPlaceTypes()+"");

                }

            }
        });

//        geoDataClient.getPlaceById("favourite_place_id_ofthe_user").addOnCompleteListener(new OnCompleteListener<PlaceBufferResponse>() {
//            @Override
//            public void onComplete(@NonNull Task<PlaceBufferResponse> task) {
//                //returns single place information
//                task.getResult().get(0).getAddress();//address of the placeid
//
//            }
//        });



//

        geoDataClient.getPlacePhotos("ChIJa147K9HX3IAR-lwiGIQv9i4").addOnCompleteListener(new OnCompleteListener<PlacePhotoMetadataResponse>() {
            @Override
            public void onComplete(@NonNull Task<PlacePhotoMetadataResponse> task) {

                geoDataClient.getPhoto(task.getResult().getPhotoMetadata().get(0)).addOnCompleteListener(new OnCompleteListener<PlacePhotoResponse>() {
                    @Override
                    public void onComplete(@NonNull Task<PlacePhotoResponse> task) {
                        Bitmap bitmap=task.getResult().getBitmap();
                        ImageView movents=findViewById(R.id.momentsiv);
//                        movents.setImageBitmap(bitmap);




                    }
                });

            }
        });




    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == PickImageActivity.PICKER_REQUEST_CODE) {
            pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (pathList != null && !pathList.isEmpty()) {
                StringBuilder sb = new StringBuilder("");
                for (int i = 0; i < pathList.size(); i++) {
                    File fileName=new File(pathList.get(i));

                    Log.i("mytag","path url " + pathList.get(i));
                    Log.i("mytag", "original file size " +fileName.length());
                    File compressed_file=saveBitmapToFile(fileName);
                    Log.i("mytag", "compressed file size " +compressed_file.length());

                    Uri uri= Uri.fromFile(compressed_file);
                    UploadTask eachUploadTask=storageReference.child(compressed_file.getName()).putFile(uri);
                    ProgressManager eachMoments=new ProgressManager();
                    eachMoments.setFileName(new File(pathList.get(i)).getName());
                    eachMoments.setUploadTask(eachUploadTask);
                    progressManagers.add(eachMoments);//todo do i have to create new eachMoments cant i override it? to save memory

                    if(!handlerRan){
                        handlerRan=true;
                        uploadHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                double totalProgress=0;

                                for(ProgressManager eachMoments:progressManagers){
                                    totalProgress+=(100*eachMoments.getUploadTask().getSnapshot().getBytesTransferred())/eachMoments.getUploadTask().getSnapshot().getTotalByteCount();
                                }
                                Log.i("mytag","total progress is "+totalProgress+" %");
                                if((int)(totalProgress/progressManagers.size())==100){
                                 uploadHandler.removeCallbacks(this);
                                }else{
                                    uploadHandler.postDelayed(this,100);

                                }
                            }
                        });
                    }



                }

//                Glide.with(getApplicationContext()).load(pathList.get(0))
//                        .asBitmap()
//                        .format(DecodeFormat.PREFER_RGB_565)
//                        .centerCrop()
//                        .into(new BitmapImageViewTarget(createbanddetails_logo) {
//                            @Override
//                            protected void setResource(Bitmap resource) {
//                                RoundedBitmapDrawable circularBitmapDrawable =
//                                        RoundedBitmapDrawableFactory.create(getApplicationContext().getResources(), resource);
//                                circularBitmapDrawable.setCircular(true);
//                            }
//                        });

//                new CustomSharedPref(getApplicationContext()).setSharedPref("band_profile_image",pathList.get(0));//default none if value is null
//                Uri uri= Uri.fromFile(new File(pathList.get(0)));
//                StorageReference storageReference= FirebaseStorage.getInstance().getReference().child("users_profile_pictures");
//                final UploadTask uploadTask=storageReference.putFile(uri);
//                uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
//                    @Override
//                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
//                        double progress=(100*taskSnapshot.getBytesTransferred())/taskSnapshot.getTotalByteCount();
////                        Log.i("mytag",progress*100+"");
//
//                    }
//                });
//
//                uploadHandler.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        double progress=(100*uploadTask.getSnapshot().getBytesTransferred())/uploadTask.getSnapshot().getTotalByteCount();
//                        Log.i("mytag",progress*100+"");
//                        uploadHandler.postDelayed(this,50);
//                    }
//                });

            }

        }
    }

    private class FragmentPageAdapter extends FragmentStatePagerAdapter{

        private List<Fragment> fragments=new ArrayList<>();

        public FragmentPageAdapter(FragmentManager fm,List<Fragment> fragments) {
            super(fm);
            this.fragments=fragments;
        }


        @Override
        public Fragment getItem(int position) {
            if(fragments.get(position).getClass()==FSlider.class){
                FSlider fSlider=new FSlider();
                Bundle bundle=new Bundle();
                bundle.putSerializable("fragment_data",users.get(position));
                fSlider.setArguments(bundle);
                fragments.set(position,fSlider);//override
            }
            return fragments.get(position);

        }

        @Override
        public int getCount() {
            return fragments.size();//return 5 pages of fragments
        }
    }

    private class ProgressManager{
        private String fileName;
        private UploadTask uploadTask;

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        public UploadTask getUploadTask() {
            return uploadTask;
        }

        public void setUploadTask(UploadTask uploadTask) {
            this.uploadTask = uploadTask;
        }
    }
    private File saveBitmapToFile(File file){
        try {

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 10;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
//            final int REQUIRED_SIZE=(int)file.length();
            final int REQUIRED_SIZE=40;//effects in resolution i.e higher is nice but size will increase
            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file#or idont have to override
//            file.createNewFile();

            //create new file in the internal app directory
            //todo refer save files on deviec storage documentation on android
            File outputFile=new File(getApplicationContext().getFilesDir(),file.getName());
            FileOutputStream outputStream = new FileOutputStream(outputFile);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 50 , outputStream);
            outputStream.close();
            return outputFile;
        } catch (Exception e) {
            return null;
        }
    }

    private void getMoments(){

        FirebaseFirestore.getInstance().collection("users").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                for(DocumentSnapshot each:queryDocumentSnapshots.getDocuments()){
                    Log.i("mytag",each.toObject(Users.class).getUserName());
                    Log.i("mytag",each.toObject(Users.class).getUserMoments());
                    Users eachUser=each.toObject(Users.class);
                    users.add(eachUser);
                }
                mPagerAdapter = new FragmentPageAdapter(getSupportFragmentManager(),fragments);
                viewPager.setAdapter(mPagerAdapter);
                viewPager.setOffscreenPageLimit(1);//only two instance of screen at once
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        //checks whether the google play service is installed or up to date or not
        int statusCode=GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getApplicationContext());
        }
}
