package com.kawadi.swornim.dropit.DataStructure;

import java.io.Serializable;

public class Users implements Serializable{
    private String userName;
    private String userStatus;
    private String userMoments;

    public Users(){}

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public String getUserMoments() {
        return userMoments;
    }

    public void setUserMoments(String userMoments) {
        this.userMoments = userMoments;
    }
}
