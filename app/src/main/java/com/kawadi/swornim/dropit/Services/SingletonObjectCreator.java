package com.kawadi.swornim.dropit.Services;


/*
If two objects are same then there hascode must be same
example object1.hashCode()==object2.hashCode()
 */

import android.content.Context;
import android.os.Environment;

import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;

import java.io.File;
import java.io.Serializable;

public class SingletonObjectCreator implements Serializable{

    private static volatile SingletonObjectCreator singletonObjectCreator;
    private static volatile SimpleCache simpleCache;
    private static volatile SimpleCache downloadSimpleCache;

    private SingletonObjectCreator(){}


    public static SingletonObjectCreator getInstance(){
        if(singletonObjectCreator==null){

            synchronized (SingletonObjectCreator.class){

                return singletonObjectCreator=new SingletonObjectCreator();

            }
        }
        return  singletonObjectCreator;
    }

    public static SimpleCache getSimpleCacheInstance(Context context){
        if(simpleCache==null){

            synchronized (SimpleCache.class){

                return simpleCache= new SimpleCache(new File(context.getCacheDir(), "media"), new NoOpCacheEvictor());//media is a folder
            }
        }
        return simpleCache;
    }

    public static SimpleCache getDownloadSimpelCacheInstance(Context context){
        if(downloadSimpleCache==null){

            synchronized (SimpleCache.class){
                File cacheDirectory=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),"Media");
                //actionFile is the file where the actions are saved like a remove action or active actions state

                return downloadSimpleCache = new SimpleCache(cacheDirectory,new NoOpCacheEvictor());
            }
        }
        return downloadSimpleCache;
    }

    protected  SingletonObjectCreator readResolve(){
        return getInstance();
    }
}


//
//     class SingletonObjectCreatorEager {
//        /*
//    This style is called eager initialization where the instance is created even though client application might not be using which leads
//    to memory leaks in some cases like database connection and all
//     */
//        private static volatile SingletonObjectCreatorEager singletonObjectCreator1 = new SingletonObjectCreatorEager();//created even though....
//
//        //private constructor so that no other classes can create the instance
//        private SingletonObjectCreatorEager() {
//        }
//
//        //public for accessing ,static because its a member of the class (as no objects will be created)
//        public static SingletonObjectCreatorEager getInstance() {
//            return singletonObjectCreator1;
//        }
//    }
//
//
//class SingletonObjectCreatorLazy{
//        /*
//    This style is called Lazy initialization where the instance is created inside the getInstance method so no memory leaks
//     */
//        private static volatile SingletonObjectCreatorLazy singletonObjectCreator2;
//
//        //private constructor so that no other classes can create the instance
//        private SingletonObjectCreatorLazy() {
//        }
//
//        //public for accessing ,static because its a member of the class (as no objects will be created)
//        public static SingletonObjectCreatorLazy getInstance() {
//            if (singletonObjectCreator2 == null) {
//                //if first time called
//                singletonObjectCreator2 = new SingletonObjectCreatorLazy();
//            }
//            return singletonObjectCreator2;
//        }
//    }
//
//
//
//class  SingletonThreadNotSafe{
//
//
///*
// code are not thead safe because if the two threads runs exactly at once by entering the getInstance method sequentially(but very close to each other i.e before one thread
//calling getInstance and actually intializing the instance gets out of the scope another thread is also inside the getInstance which also sees instance being not initiliazed then
//the singleleton pattern will be broken and two instance will be created
// */
//
//    public static  void main(String[] args){
//        Thread thread1=new Thread(new Runnable() {
//            @Override
//            public void run() {
//                //create instance1=Singleton.getInstance()
//
//            }
//        });
//
//        Thread thread2=new Thread(new Runnable() {
//            @Override
//            public void run() {
//                //create instance2=Singleton.getInstance()
//                //instance2.hashCode!=instance1.hashCode()
//            }
//        });
//
//        //run both of them at once( almost)
//
//        thread1.start();
//        thread2.start();
//    }
//}
//
//
// class SingletonThreadSafeButPerformance {
//
//     /*
//For the thread safe we have the solution of using the synchronized keyword which basically  makes the function synchronized i.e blocking which means if any one calling person( threads or processes or other classes instance) calls that synchronized method then unless that calling person is out of the scope of synchronized method other calling persons cannot enter that code or cannot call the method ,they have to wait in a queue .This solves the problem of Threads in above code but raises a new problem that is performance issues because all of the callers has to wait
//due to locking methods
//     */
//
//
//    private static volatile SingletonThreadSafeButPerformance singletonObjectCreator4;
//
//    //private constructor so that no other classes can create the instance
//    private SingletonThreadSafeButPerformance() {
//    }
//
//    //public for accessing ,static because its a member of the class (as no objects will be created)
//    public static synchronized SingletonThreadSafeButPerformance getInstance() {
//        if (singletonObjectCreator4 == null) {
//            //if first time called
//            singletonObjectCreator4 = new SingletonThreadSafeButPerformance();
//        }
//        return singletonObjectCreator4;
//    }
//}
//
//
//
//class SingletonThreadSafeAndPerformance {
//
//    /*
//This style is called Lazy initialization where the instance is created inside the getInstance method so no memory leaks
// */
//
//    /*
//
//Solutions:
//For the first caller make the synchronized method blocking then for others dont make the method calling by wrapping a if else statement i.e use double checking locking
//
//        */
//    private static volatile SingletonThreadSafeAndPerformance singletonObjectCreator5;
//
//    //private constructor so that no other classes can create the instance
//    private SingletonThreadSafeAndPerformance() {
//    }
//
//    //public for accessing ,static because its a member of the class (as no objects will be created)
//    public static SingletonThreadSafeAndPerformance getInstance() {
//
//        //if not first time then others dont have to wait for being blocked to access the singleton instance
//        if(singletonObjectCreator5==null){
//            //if first time then make the scope synchronized for this class
//
//            synchronized (SingletonThreadSafeAndPerformance.class){
//                return singletonObjectCreator5=new SingletonThreadSafeAndPerformance();
//            }
//
//
//        }
//        return singletonObjectCreator5;
//    }
//}
//
//
//
//class SingletonDeserializationSafe implements Serializable{
//    /*
//Above SingletonThreadSafeAndPerformance class will be still broken for same thread accessing the synchronized methods at once because the other thread may read partial data
//Without volatile the another thread in java can see half of the initiliazed states of the singletonInstance but
//with volatile read is permitted untill all the writes are totally guaranted for that instance
//
// */
//
//
//
///*
//Above code is thread safe,nice performance and is volatile protected but not Serialization Safe
//This means distributed application can have same objects in the file(serialized form) but not different objects
//
//Why this happenes because :whenever we serialize the object it is fine but whenever we deserialize the object then it creates new instance of the object even though the deseriailization
//is of original singleton object
//
//Solution: implement readResolve() and return instance; it sures that no one can create new instance when deserialization is done
//
// */
//
//
//
//
//
//    //volatile protects from reading partially
//    private static volatile SingletonDeserializationSafe singletonObjectCreator6;
//
//    //private constructor so that no other classes can create the instance
//    private SingletonDeserializationSafe() {
//    }
//
//    //public for accessing ,static because its a member of the class (as no objects will be created)
//    public static SingletonDeserializationSafe getInstance() {
//
//        //if not first time then others dont have to wait for being blocked to access the singleton instance
//        if(singletonObjectCreator6==null){
//            //if first time then make the scope synchronized for this class
//
//            synchronized (SingletonDeserializationSafe.class){
//                return singletonObjectCreator6=new SingletonDeserializationSafe();
//            }
//
//
//        }
//        return singletonObjectCreator6;
//    }
//
//    protected  SingletonDeserializationSafe readResolve(){
//        return getInstance();
//    }
//}
//





