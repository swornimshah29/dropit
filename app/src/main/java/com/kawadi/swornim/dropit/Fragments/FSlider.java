package com.kawadi.swornim.dropit.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.offline.DownloadAction;
import com.google.android.exoplayer2.offline.DownloadService;
import com.google.android.exoplayer2.offline.Downloader;
import com.google.android.exoplayer2.offline.DownloaderConstructorHelper;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.dash.manifest.DashManifest;
import com.google.android.exoplayer2.source.dash.offline.DashDownloader;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSource;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.upstream.cache.CacheDataSink;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheUtil;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;
import com.kawadi.swornim.dropit.Activity.MainActivity;
import com.kawadi.swornim.dropit.DataStructure.Users;
import com.kawadi.swornim.dropit.R;
import com.kawadi.swornim.dropit.Services.DropItDownloadService;
import com.kawadi.swornim.dropit.Services.ProgressiveDownloadAction;
import com.kawadi.swornim.dropit.Services.SingletonObjectCreator;

import java.io.File;
import java.io.IOException;
import java.util.Collections;

public class FSlider extends Fragment implements Player.EventListener{
    private TextView userStatustv;
    private TextView userNametv;
    private ImageView userProfile;
    private PlayerView playerView;
    private SimpleExoPlayer exoPlayer;
    private Users object;
    private static  SimpleCache simpleCache;

    private Integer[] colorValues={
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fslider_main,container,false);
        userStatustv=view.findViewById(R.id.fslider_main_userStatus);
        userNametv=view.findViewById(R.id.fslider_main_profileName_tv);
        playerView=view.findViewById(R.id.playerView);

        //get the arguments
        object= (Users) getArguments().getSerializable("fragment_data");
        if(object!=null){
            userStatustv.setText(object.getUserStatus());
            userNametv.setText(object.getUserName());
        }


//        exoPlayer= ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(getActivity()),new DefaultTrackSelector(),new DefaultLoadControl());
        SimpleExoPlayer exoPlayer = ExoPlayerFactory.newSimpleInstance(getContext(), new DefaultTrackSelector());

        playerView.setPlayer(exoPlayer);

//        Uri uri= Uri.parse("https://www.radiantmediaplayer.com/media/bbb-360p.mp4");
        Uri uri= Uri.parse(object.getUserMoments());
//        MediaSource mediaSource=new ExtractorMediaSource.Factory(new DefaultHttpDataSourceFactory("your_string")).createMediaSource(uri);

//        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
//        TrackSelection.Factory videoTrackSelectionFactory =
//                new AdaptiveTrackSelection.Factory(bandwidthMeter);
//        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        MediaSource mediaSource = new ExtractorMediaSource(uri,
                new CacheDataSourceFactory(getContext(), 100 * 1024 * 1024, 20 * 1024 * 1024), new DefaultExtractorsFactory(), null,null);
        exoPlayer.addListener(this);
        byte[] data=new byte[]{};

        exoPlayer.prepare(mediaSource,false,false);
        exoPlayer.setPlayWhenReady(true);
        SingletonObjectCreator singletonObjectCreator=SingletonObjectCreator.getInstance();
        Log.i("mytag","Singleton hashcode in fslider"+singletonObjectCreator.hashCode());

        return  view;
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

        Log.i("mytag","onLoadingChanged "+isLoading);

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        Log.i("mytag","onPlayerError  "+error.getMessage());
        //one of the error is that if one simplecache instance uses the same folder then another instance cannot access that folder

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    public class CacheDataSourceFactory implements DataSource.Factory {

        private final Context context;
        private final DefaultDataSourceFactory defaultDatasourceFactory;
        private final long maxFileSize, maxCacheSize;

        CacheDataSourceFactory(Context context, long maxCacheSize, long maxFileSize) {
            super();
            this.context = context;
            this.maxCacheSize = maxCacheSize;
            this.maxFileSize = maxFileSize;
            String userAgent = Util.getUserAgent(context, context.getString(R.string.app_name));
            DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            defaultDatasourceFactory = new DefaultDataSourceFactory(this.context,
                    bandwidthMeter,
                    new DefaultHttpDataSourceFactory(userAgent, bandwidthMeter));


            //other options for defaultDatasourceFactory :
        }

        @Override
        public DataSource createDataSource() {
            LeastRecentlyUsedCacheEvictor evictor1 = new LeastRecentlyUsedCacheEvictor(maxCacheSize);//type of caching where most used datas are kept and least are dropped
            NoOpCacheEvictor evictor2 = new NoOpCacheEvictor();//
//            new File(getActivity().getFilesDir(), "media")


            /*
            Works perfectly with the evictor2
             */

//            CacheDataSource cacheDataSource= new CacheDataSource(simpleCache, defaultDatasourceFactory.createDataSource(),
//                    new FileDataSource(), new CacheDataSink(simpleCache, maxFileSize),
//                    CacheDataSource.FLAG_BLOCK_ON_CACHE | CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR, new CacheDataSource.EventListener() {
//                @Override
//                public void onCachedBytesRead(long cacheSizeBytes, long cachedBytesRead) {
//                    Log.i("mytag","total cache size for "+object.getUserName()+" "+cacheSizeBytes);
//                    Log.i("mytag","total cache size read for "+object.getUserName()+" "+cachedBytesRead);
//                    //total cache read for is the bytes that is reading from the disk it starts from 1024 to .. actual video size value changes after next read
//                }
//            });
            SimpleCache simpleCache=SingletonObjectCreator.getSimpleCacheInstance(MainActivity.context);
            CacheDataSource cacheDataSource = new CacheDataSource(simpleCache, defaultDatasourceFactory.createDataSource(),
                    new FileDataSource(), new CacheDataSink(simpleCache, maxCacheSize),
                    CacheDataSource.FLAG_BLOCK_ON_CACHE |CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR, null);

//            Log.i("mytag","url of the data source for "+object.getUserName()+" "+cacheDataSource.getUri());
//
//            Log.i("mytag","keys for "+object.getUserName()+" "+simpleCache.getKeys());
//            Log.i("mytag","dataspec key for "+object.getUserName()+" "+new DataSpec(cacheDataSource.getUri()).key);
//            Log.i("mytag","dataspec length for "+object.getUserName()+" "+new DataSpec(cacheDataSource.getUri()).length);
//            Log.i("mytag","dataspec postBody for "+object.getUserName()+" "+new DataSpec(cacheDataSource.getUri()).postBody);
//            Log.i("mytag","Total disk space consumed by the all the caches "+object.getUserName()+" "+simpleCache.getCacheSpace());


            return cacheDataSource;

            //CacheDataSource has implemented DataSource interface
            //evictor means to throw away or to remove because of old never used cached datas
        }
    }


}

