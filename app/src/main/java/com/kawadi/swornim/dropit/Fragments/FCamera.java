package com.kawadi.swornim.dropit.Fragments;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.exoplayer2.offline.DownloadAction;
import com.google.android.exoplayer2.offline.DownloadManager;
import com.google.android.exoplayer2.offline.DownloadService;
import com.google.android.exoplayer2.offline.Downloader;
import com.google.android.exoplayer2.offline.DownloaderConstructorHelper;
import com.google.android.exoplayer2.offline.ProgressiveDownloader;
import com.google.android.exoplayer2.offline.StreamKey;
import com.google.android.exoplayer2.scheduler.PlatformScheduler;
import com.google.android.exoplayer2.scheduler.Scheduler;
import com.google.android.exoplayer2.source.smoothstreaming.offline.SsDownloader;
import com.google.android.exoplayer2.ui.DownloadNotificationUtil;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata;
import com.google.firebase.ml.vision.common.FirebaseVisionPoint;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceLandmark;
import com.kawadi.swornim.dropit.Activity.MainActivity;
import com.kawadi.swornim.dropit.Interface.IDownloadManagerListener;
import com.kawadi.swornim.dropit.R;
import com.kawadi.swornim.dropit.Services.DropItDownloadService;
import com.kawadi.swornim.dropit.Services.ProgressiveDownloadAction;
import com.kawadi.swornim.dropit.Services.SingletonObjectCreator;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FCamera extends Fragment implements SurfaceHolder.Callback,View.OnClickListener,View.OnLongClickListener,View.OnTouchListener,IDownloadManagerListener {
    private SurfaceHolder mHolder;
    private SurfaceView mSurfaceView;
    private Camera mCamera;
    private Context mContext;
    private ImageView fcamera_main_cameraIcon;
    private MediaRecorder mMediaRecorder;
    private static int MEDIA_TYPE_VIDEO=0;
    private static int MEDIA_TYPE_IMAGE=1;
    private boolean isSurfaceCreated=false;
    private Handler videoCaptureHandler=null;
    private Runnable videoCaptureRunnable=null;
    private boolean isVideoCapturing=false;
    private FirebaseVisionImage image;
    private FirebaseVisionImageMetadata imageMetadata;
    private FirebaseVisionFaceDetectorOptions options;
    private DownloadManager downloadManager;
    private TextView details;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeDownloadManager();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView=inflater.inflate(R.layout.fcamera_main,container,false);
        mSurfaceView=mView.findViewById(R.id.fcamera_main_surfaceView);
        fcamera_main_cameraIcon=mView.findViewById(R.id.fcamera_main_cameraIcon);
        fcamera_main_cameraIcon.setOnClickListener(this);
        fcamera_main_cameraIcon.setOnLongClickListener(this);
        details=mView.findViewById(R.id.details);
        fcamera_main_cameraIcon.setOnTouchListener(this);
        mHolder=mSurfaceView.getHolder();
        mHolder.addCallback(this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                startDownloading();
            }
        }).start();
        return mView;
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.i("mytag","surfaceCreated ");
//        isSurfaceCreated=true;
//        mCamera=getCameraInstance();
//        mCamera.setDisplayOrientation(90);
//        Camera.Parameters parameters=mCamera.getParameters();
//        parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//            mCamera.enableShutterSound(false);
//        }
//
//        mCamera.setParameters(parameters);
//        videoCaptureHandler=new Handler();
//        videoCaptureRunnable=new Runnable() {
//            @Override
//            public void run() {
//                if (mCamera != null && isSurfaceCreated) {
//                    if(!isVideoCapturing) {
//                        Log.i("mytag","Video Capturing starting");
//                        isVideoCapturing=true;
//                        mMediaRecorder = new MediaRecorder();
//                        //unlock and set the camera to mrecorder
//                        mCamera.unlock();
//
//                        mMediaRecorder.setCamera(mCamera);
//
//                        //set sources hardware specific
//                        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
//                        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
//
//
//                        //set the camcorderprofile (api>=8)
//                        mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
//                        //set the output file
//                        try {
//                            mMediaRecorder.setOutputFile(getOutputMediaFile(MEDIA_TYPE_VIDEO).toString());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                        mMediaRecorder.setPreviewDisplay(mHolder.getSurface());
//                        try {
//                            mMediaRecorder.prepare();
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                        mMediaRecorder.start();
//                        videoCaptureHandler.postDelayed(videoCaptureRunnable,10000);
//                    }else{
//                        //stop the video capturing since timeout of 10000 seconds
//                        videoCaptureHandler.removeCallbacks(videoCaptureRunnable);
//                        mMediaRecorder.stop();//this will save the video on the disk and stop
//                        mMediaRecorder.release();
//                        mCamera.lock();
//                        isVideoCapturing=false;//reset
//                        Log.i("mytag","Video Capturing saved");
//
//                    }
//                }else{
//                    Log.i("mytag","onsurface not created or mcamera is null");
//                }
//            }
//        };
//        RequestQueue requestQueue= Volley.newRequestQueue(getContext());
//        StringRequest stringRequest=new StringRequest(Request.Method.POST,
////                IMetaData.serverUrl+"/authentication.php",
//                "https://maps.googleapis.com/maps/api/elevation/json?locations=27.6788727,85.364918&key=AIzaSyC_ozu7kpsXiFTjBl_vz6yfNFoGxSBJRqk",
//                new com.android.volley.Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        Log.i("mytag","rating response "+response);
//                        details.setText(response);
//                    }
//                }, new com.android.volley.Response.ErrorListener() {
//
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//            }
//        }){
//            @Override
//            protected java.util.Map<String, String> getParams() throws AuthFailureError {
//                Map<String,String> map=new HashMap<>();
//
//                return map;
//            }
//
//
//        };
//
//        requestQueue.add(stringRequest);




    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        Log.i("mytag","surfaceChanged ");

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        isSurfaceCreated=false;
        Log.i("mytag","surfaceDestroyed");
        if(mCamera!=null){
            mCamera.release();
        }

    }


    private static Camera getCameraInstance(){
        Camera cameraInstance=null;

        try{
            if(getFrontBackCameraInstanceId("back")!=-1000){
                cameraInstance=Camera.open();
            }
            cameraInstance=Camera.open();

        }catch (Exception e){
            //camera instance is not free or some error
        }

        return cameraInstance;

    }

    private static int getFrontBackCameraInstanceId(String whichSide) {

        int numberOfCamera = Camera.getNumberOfCameras();
        int cameraInstanceId =-1000;//error no front faceing nor backfacing
        for (int i = 0; i < numberOfCamera; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (whichSide.equals("front")) {
                if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                   cameraInstanceId=i;
                   break;
                }
            } else if (whichSide.equals("back")) {
                if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                    cameraInstanceId=i;
                    break;
                }
            }

        }
        return cameraInstanceId;

    }

    private File getOutputMediaFile(int type) throws IOException {
        File mediaStorageDir=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"DROPIT");
        if(!mediaStorageDir.exists()){
            if(!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        File mediFile=new File(mediaStorageDir.getPath()+"newVideo.mp4");
        if(!mediFile.exists()){
            Log.i("mytag","Media file doesnot exists");
            mediFile.createNewFile();
        }
        if(!mediFile.exists()){
            Log.i("mytag","Media file doesnot exists");
            mediFile.createNewFile();
        }
        return  mediFile;
    }


    @Override
    public void onClick(View v) {
        //take the picture

//        DisplayMetrics displayMetric=new DisplayMetrics();
//        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetric);
//        final int heght=displayMetric.heightPixels;
//        final int width=displayMetric.widthPixels;
//
//        options=new FirebaseVisionFaceDetectorOptions.Builder()
//                .setModeType(FirebaseVisionFaceDetectorOptions.ACCURATE_MODE)
//                .setLandmarkType(FirebaseVisionFaceDetectorOptions.ALL_LANDMARKS)
//                .setMinFaceSize(0.15f)
//                .setTrackingEnabled(true)
//                .setClassificationType(FirebaseVisionFaceDetectorOptions.ALL_CLASSIFICATIONS)
//                .build();
//
//        if(mCamera!=null && isSurfaceCreated){
//            mCamera.lock();
//
//            try {
//                mCamera.setPreviewDisplay(mHolder);
//                mCamera.startPreview();
//                mCamera.setPreviewCallback(new Camera.PreviewCallback() {
//                    @Override
//                    public void onPreviewFrame(byte[] data, Camera camera) {
//                        //send this continous data to the firebase face detection
//
//                        imageMetadata=new FirebaseVisionImageMetadata.Builder()
//                                .setFormat(FirebaseVisionImageMetadata.IMAGE_FORMAT_NV21)
//                                .setHeight(heght)
//                                .setWidth(width)
//                                .setRotation(FirebaseVisionImageMetadata.ROTATION_90)
//                                .build();
//
//                        image=FirebaseVisionImage.fromByteArray(data,imageMetadata);
//
//
//                        FirebaseVisionFaceDetector faceDetector= FirebaseVision.getInstance().getVisionFaceDetector(options);
//
//                        Task<List<FirebaseVisionFace>> result=faceDetector.detectInImage(image).addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionFace>>() {
//                            @Override
//                            public void onSuccess(List<FirebaseVisionFace> firebaseVisionFaces) {
//                                Log.i("mytag","Successfully running the face detection ");
//
//                                for(FirebaseVisionFace face:firebaseVisionFaces){
//                                    Rect bounds =face.getBoundingBox();
//                                    float rotY=face.getHeadEulerAngleY();//head is roated to the right roty degree
//                                    float rotZ=face.getHeadEulerAngleZ();//head is titled sideways rotz degrees
//
//                                    //if landmarks are enable then find them as well
//                                    FirebaseVisionFaceLandmark leftEar=face.getLandmark(FirebaseVisionFaceLandmark.LEFT_EAR);
//                                    if(leftEar!=null){
//                                        FirebaseVisionPoint leftEarPos=leftEar.getPosition();
//                                        Log.i("mytag","User left ear position X "+leftEarPos.getX());
//                                        Log.i("mytag","User left ear position Y "+leftEarPos.getY());
//                                        Log.i("mytag","User left ear position Z "+leftEarPos.getZ());
//
//                                    }
//                                    //if classification was enabled then
//                                    if(face.getSmilingProbability()!=FirebaseVisionFace.UNCOMPUTED_PROBABILITY){
//                                        Log.i("mytag","User probabilty of smile is "+face.getSmilingProbability());
//                                    }
//
//                                    if(face.getRightEyeOpenProbability()!=FirebaseVisionFace.UNCOMPUTED_PROBABILITY){
//                                        Log.i("mytag","User probabilty of right eye open is "+face.getRightEyeOpenProbability());
//                                    }
//                                    Log.i("mytag","User probabilty of smile is "+face.getSmilingProbability());
//
//                                }
//
//
//                            }
//                        }).addOnFailureListener(new OnFailureListener() {
//                            @Override
//                            public void onFailure(@NonNull Exception e) {
//                                Log.i("mytag","error in the face detection "+e.getMessage());
//                            }
//                        });
//
//
//                    }
//                });
//
//            }catch (IOException e) {
//                e.printStackTrace();
//            }
//
//        }

    }

    @Override
    public boolean onLongClick(View v) {
        //start recording the videos
        Log.i("mytag","onLongClick called");

        //todo when clicked long or not long ontouchMethod is called first

        //start capturing the videos
        videoCaptureHandler.post(videoCaptureRunnable);
        return false;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        //when the users touches and leaves the view
        if(v.getId()==fcamera_main_cameraIcon.getId()){
            if(event.getAction()==MotionEvent.ACTION_DOWN){
                //when the users first time touches start couting the timestamp like snapchat
                Log.i("mytag","MotionEvent.ACTION_DOWN");

            }else if(event.getAction()==MotionEvent.ACTION_UP){
                //user has released the touch
                Log.i("mytag","MotionEvent.ACTION_UP");
                //if it exists >10 then already removecallbacks has been called soo check for null or not
                videoCaptureHandler.removeCallbacks(videoCaptureRunnable);

            }else if(event.getAction()==MotionEvent.ACTION_MOVE){
                //when you move your fingers touching and dragging on the screen
            }
        }


        return false;
    }


    private  void downloadServiceVideoCache() throws IOException, InterruptedException {
        File file=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),"downloadedContent");
        if(!file.exists()){
            file.mkdir();
        }
        SimpleCache simpleCache=new SimpleCache(file,new NoOpCacheEvictor());
        DefaultHttpDataSourceFactory factory=new DefaultHttpDataSourceFactory("Exoplayer", new TransferListener() {
            @Override
            public void onTransferInitializing(DataSource dataSource, DataSpec dataSpec, boolean b) {

            }

            @Override
            public void onTransferStart(DataSource dataSource, DataSpec dataSpec, boolean b) {
                Log.i("mytag","OnTrasferStart datasource url "+dataSource.getUri()+"");
                Log.i("mytag","OnTrasferStart datasource url encoded path "+dataSource.getUri().getEncodedPath()+"");
                Log.i("mytag","OnTrasferStart datasource url host "+dataSource.getUri().getHost()+"");
                Log.i("mytag","OnTrasferStart datasource url path "+dataSource.getUri().getPath()+"");
                Log.i("mytag","OnTrasferStart datasource url userinformation "+dataSource.getUri().getUserInfo()+"");

                Log.i("mytag","OnTrasferStart dataSpec key"+dataSpec.key);
                Log.i("mytag","OnTrasferStart dataSpec postBody"+dataSpec.postBody);
                Log.i("mytag","OnTrasferStart dataSpec uri"+dataSpec.uri);
                Log.i("mytag","OnTrasferStart dataSpec length"+dataSpec.length);
                Log.i("mytag","OnTrasferStart dataSpec absoluteStreamPosition"+dataSpec.absoluteStreamPosition);
            }

            @Override
            public void onBytesTransferred(DataSource dataSource, DataSpec dataSpec, boolean b, int i) {
                Log.i("mytag","onBytesTransferred datasource url "+dataSource.getUri()+"");
                Log.i("mytag","onBytesTransferred datasource url encoded path "+dataSource.getUri().getEncodedPath()+"");
                Log.i("mytag","onBytesTransferred datasource url host "+dataSource.getUri().getHost()+"");
                Log.i("mytag","onBytesTransferred datasource url path "+dataSource.getUri().getPath()+"");
                Log.i("mytag","onBytesTransferred datasource url userinformation "+dataSource.getUri().getUserInfo()+"");

                Log.i("mytag","onBytesTransferred bytesTransferred "+i);
            }

            @Override
            public void onTransferEnd(DataSource dataSource, DataSpec dataSpec, boolean b) {

            }

        });

        DownloaderConstructorHelper downloaderConstructorHelper=new DownloaderConstructorHelper(simpleCache,factory);

        final SsDownloader dashDownloader=new SsDownloader(Uri.parse("https://firebasestorage.googleapis.com/v0/b/dropit-bd0a6.appspot.com/o/users_moments%2FSnapchat-1814254895.mp4?alt=media&token=3ff23a67-6e99-4eb1-a668-86230c91ad6b"), Collections.singletonList(new StreamKey(0, 0, 0)),downloaderConstructorHelper);
        //perform the download
        dashDownloader.download();

        //you can also access the cachedatasource
        CacheDataSource cacheDataSource=new CacheDataSource(simpleCache,factory.createDataSource(),CacheDataSource.FLAG_BLOCK_ON_CACHE);

        //later pass this instance to the exoplayer to play the cache data

        //user can also remove the cache file as

//        DataSpec dataSpec=new DataSpec(Uri.parse(""),0,1024,null);
//        CacheUtil.remove(simpleCache,CacheUtil.getKey(dataSpec));

    }

    private void startDownloading() {
        DownloadAction downloadAction = new ProgressiveDownloadAction(Uri.parse(" https://firebasestorage.googleapis.com/v0/b/dropit-bd0a6.appspot.com/o/users_moments%2FSnapchat-1814254895.mp4?alt=media&token=3ff23a67-6e99-4eb1-a668-86230c91ad6b"), false, null, null);
        DownloadAction downloadAction1=new ProgressiveDownloadAction(Uri.parse(" https://firebasestorage.googleapis.com/v0/b/dropit-bd0a6.appspot.com/o/users_moments%2FSnapchat-391675001.mp4?alt=media&token=e4c25c7c-2f54-45cf-871c-069c9221475d"), false, null, null);
        DownloadService.startWithAction(MainActivity.context, DropItDownloadService.class, downloadAction1, true);
        DownloadService.startWithAction(MainActivity.context, DropItDownloadService.class, downloadAction, true);


        //downloadAction.createDownloader();//creates another instance of download streams for another url2
        DropItDownloadService.setIDownloadManagerListener(this);

        }


    @Override
    public void downloadManagerCalled() {
        //service class calls this when the downloadmanager has been initlized
        if (DropItDownloadService.downloadManager != null) {

            DropItDownloadService.downloadManager.addListener(new DownloadManager.Listener() {
                @Override
                public void onInitialized(DownloadManager downloadManager) {
                    Log.i("mytag", "Download manager has been intialized by the system");

                }

                @Override
                public void onTaskStateChanged(DownloadManager downloadManager, DownloadManager.TaskState taskState) {
                    Log.i("mytag", "Download manager task state is changed");
                    Log.i("mytag", "Numbers of downloads " + DropItDownloadService.downloadManager.getDownloadCount());
                    Log.i("mytag", "Numbers of task on going " + DropItDownloadService.downloadManager.getTaskCount());

                    for (DownloadManager.TaskState eachTaskState : downloadManager.getAllTaskStates()) {
                        Log.i("mytag", "taskid " + eachTaskState.taskId);
                        Log.i("mytag", "state " + eachTaskState.state);
                        Log.i("mytag", "percentage downloaded " + eachTaskState.downloadPercentage);
                        Log.i("mytag", "---------------------------------------------------------------------------------------------------------");

                    }

                }

                @Override
                public void onIdle(DownloadManager downloadManager) {

                }
            });

        }
    }




    private void initializeDownloadManager(){
        DefaultHttpDataSourceFactory defaultHttpDataSourceFactory=new DefaultHttpDataSourceFactory("exoplayer", new TransferListener() {
            @Override
            public void onTransferInitializing(DataSource dataSource, DataSpec dataSpec, boolean b) {
                Log.i("mytag","onTransferInitializing "+dataSpec.length);
                Log.i("mytag","onTransferInitializing "+dataSpec.uri);

            }

            @Override
            public void onTransferStart(DataSource dataSource, DataSpec dataSpec, boolean b) {
                Log.i("mytag","onTransferStart "+dataSource.getUri());
                Log.i("mytag","onTransferStart "+dataSource.getResponseHeaders());

            }

            @Override
            public void onBytesTransferred(DataSource dataSource, DataSpec dataSpec, boolean b, int i) {
                Log.i("mytag","onBytesTransferred "+b);
                Log.i("mytag","onBytesTransferred "+i);

            }

            @Override
            public void onTransferEnd(DataSource dataSource, DataSpec dataSpec, boolean b) {
                Log.i("mytag","onTransferEnd "+b);

            }
        });

        File actionFile=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),"ActionFile");
        SingletonObjectCreator singletonObjectCreator=SingletonObjectCreator.getInstance();
        Log.i("mytag","Singleton hashcode in service "+singletonObjectCreator.hashCode());
        DownloaderConstructorHelper downloaderConstructorHelper=new DownloaderConstructorHelper(
                SingletonObjectCreator.getDownloadSimpelCacheInstance(MainActivity.context),defaultHttpDataSourceFactory);
        downloadManager=new DownloadManager(downloaderConstructorHelper ,actionFile, com.google.android.exoplayer2.offline.ProgressiveDownloadAction.DESERIALIZER);


    }

}
