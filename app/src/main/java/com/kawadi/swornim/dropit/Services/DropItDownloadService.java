package com.kawadi.swornim.dropit.Services;

/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.app.Notification;
import android.opengl.Visibility;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.view.View;

import com.google.android.exoplayer2.offline.DownloadManager;
import com.google.android.exoplayer2.offline.DownloadManager.TaskState;
import com.google.android.exoplayer2.offline.DownloadService;
import com.google.android.exoplayer2.offline.DownloaderConstructorHelper;
import com.google.android.exoplayer2.offline.ProgressiveDownloadAction;
import com.google.android.exoplayer2.scheduler.PlatformScheduler;
import com.google.android.exoplayer2.ui.DownloadNotificationUtil;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.NotificationUtil;
import com.google.android.exoplayer2.util.Util;
import com.kawadi.swornim.dropit.Activity.MainActivity;
import com.kawadi.swornim.dropit.Interface.IDownloadManagerListener;
import com.kawadi.swornim.dropit.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/** A service for downloading media. */
public class DropItDownloadService extends DownloadService {

    private static final String CHANNEL_ID = "download_channel";
    private static final int JOB_ID = 1;
    private static final int FOREGROUND_NOTIFICATION_ID = 1;
    public static DownloadManager downloadManager=null;
    public static List<IDownloadManagerListener> listeners=new ArrayList<>();

    public DropItDownloadService() {
        super(
                FOREGROUND_NOTIFICATION_ID,
                DEFAULT_FOREGROUND_NOTIFICATION_UPDATE_INTERVAL,
                CHANNEL_ID,
                R.string.exo_download_notification_channel_name);
        initializeDownloadManager();

    }

    @Override
    protected DownloadManager getDownloadManager() {

        //call all the listener classes
        for(IDownloadManagerListener each:listeners){
            each.downloadManagerCalled();//call all the classes
        }


        return  downloadManager;
    }

    @Override
    protected PlatformScheduler getScheduler() {
        return Util.SDK_INT >= 21 ? new PlatformScheduler(this, JOB_ID) : null;

    }

    @Override
    protected Notification getForegroundNotification(TaskState[] taskStates) {
//        Notification notification=DownloadNotificationUtil.buildProgressNotification(  /* context= */ this,
//                R.drawable.exo_controls_play,
//                CHANNEL_ID,
//                /* contentIntent= */ null,
//                /* message= */ null,taskStates);
//        NotificationManagerCompat notificationManagerCompat=NotificationManagerCompat.from(this);
//        notificationManagerCompat.notify(100,notification);
//        notificationManagerCompat.cancel(100);

        NotificationCompat.Builder builder=new NotificationCompat.Builder(this,null)
                .setAutoCancel(true)
                .setContentTitle("New Moments on the way")
                .setVisibility(View.INVISIBLE)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManagerCompat=NotificationManagerCompat.from(this);
        Notification notification=builder.build();

        return notification;

    }

    @Override
    protected void onTaskStateChanged(TaskState taskState) {
        if (taskState.action.isRemoveAction) {
            return;
        }

//        Notification notification = null;
//        if (taskState.state == TaskState.STATE_COMPLETED) {
//            Log.i("mytag","downloaded completed");
//            notification =
//                    DownloadNotificationUtil.buildDownloadCompletedNotification(
//                            /* context= */ this,
//                            R.drawable.exo_controls_play,
//                            CHANNEL_ID,
//                            /* contentIntent= */ null,
//                            Util.fromUtf8Bytes(taskState.action.data));
//        } else if (taskState.state == TaskState.STATE_FAILED) {
//            Log.i("mytag","downloaded failed");
//
//            notification =
//                    DownloadNotificationUtil.buildDownloadFailedNotification(
//                            /* context= */ this,
//                            R.drawable.exo_controls_play,
//                            CHANNEL_ID,
//                            /* contentIntent= */ null,
//                            Util.fromUtf8Bytes(taskState.action.data));
//        }
//        int notificationId = FOREGROUND_NOTIFICATION_ID + 1 + taskState.taskId;
//        NotificationUtil.setNotification(this, notificationId, notification);
    }

    private void initializeDownloadManager(){
        DefaultHttpDataSourceFactory defaultHttpDataSourceFactory=new DefaultHttpDataSourceFactory("exoplayer", new TransferListener() {
            @Override
            public void onTransferInitializing(DataSource dataSource, DataSpec dataSpec, boolean b) {
                Log.i("mytag","onTransferInitializing "+dataSpec.length);
                Log.i("mytag","onTransferInitializing "+dataSpec.uri);

            }

            @Override
            public void onTransferStart(DataSource dataSource, DataSpec dataSpec, boolean b) {
                Log.i("mytag","onTransferStart "+dataSource.getUri());
                Log.i("mytag","onTransferStart "+dataSource.getResponseHeaders());

            }

            @Override
            public void onBytesTransferred(DataSource dataSource, DataSpec dataSpec, boolean b, int i) {
                Log.i("mytag","onBytesTransferred "+b);
                Log.i("mytag","onBytesTransferred "+i);

            }

            @Override
            public void onTransferEnd(DataSource dataSource, DataSpec dataSpec, boolean b) {
                Log.i("mytag","onTransferEnd "+b);

            }
        });

        File actionFile=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),"ActionFile");
        SingletonObjectCreator singletonObjectCreator=SingletonObjectCreator.getInstance();
        Log.i("mytag","Singleton hashcode in service "+singletonObjectCreator.hashCode());
        DownloaderConstructorHelper downloaderConstructorHelper=new DownloaderConstructorHelper(
                SingletonObjectCreator.getDownloadSimpelCacheInstance(MainActivity.context),defaultHttpDataSourceFactory);
        downloadManager=new DownloadManager(downloaderConstructorHelper ,actionFile, ProgressiveDownloadAction.DESERIALIZER);


    }

    public static void setIDownloadManagerListener(IDownloadManagerListener downloadManagerListener){
        listeners.add(downloadManagerListener);//add the classes that wants the service
    }





}