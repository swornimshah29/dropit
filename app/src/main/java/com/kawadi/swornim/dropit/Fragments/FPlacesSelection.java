package com.kawadi.swornim.dropit.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kawadi.swornim.dropit.R;

import java.util.ArrayList;
import java.util.List;

public class FPlacesSelection extends Fragment {
    private RecyclerView recyclerView;
    private RecyclerViewAdapter recyclerViewAdapter;
    private List<String> images=new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fplace_selection,container,false);
        recyclerView=view.findViewById(R.id.recyclerviewid);
        recyclerViewAdapter=new RecyclerViewAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(recyclerViewAdapter);
        images.add("adasdas");
        images.add("assadasdadasd sadsad");
        images.add("hello and hi");
        return view;
    }


    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_add_users,parent,false);
            //custom class constructor called once because oncreateviewholder gets called once
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            //similar to getview

            holder.textView.setText(images.get(position));
            //use contextcompat for api >=22
            holder.imageView.setImageResource(R.drawable.patter);
        }

        @Override
        public int getItemCount() {
            return images.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{
            /*
            This class holds the widgets(imageview,textview entire .xml) in the memory this is why its called viewholder
             */
            public ImageView imageView;
            public TextView textView;


            public ViewHolder(View itemView) {
                super(itemView);
                //create reference once
                textView=itemView.findViewById(R.id.addphoto);
                imageView=itemView.findViewById(R.id.addphoto1);

            }
        }
    }
}
