package com.kawadi.swornim.dropit.Interface;

public interface IRegistration {
    void makeRegistration();
    boolean isRegistered();
}
